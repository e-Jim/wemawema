var domIsReady = (function(domIsReady) {
   var isBrowserIeOrNot = function() {
      return (!document.attachEvent || typeof document.attachEvent === "undefined" ? 'not-ie' : 'ie');
   }

   domIsReady = function(callback) {
      if(callback && typeof callback === 'function'){
         if(isBrowserIeOrNot() !== 'ie') {
            document.addEventListener("DOMContentLoaded", function() {
               return callback();
            });
         } else {
            document.attachEvent("onreadystatechange", function() {
               if(document.readyState === "complete") {
                  return callback();
               }
            });
         }
      } else {
         console.error('The callback is not a function!');
      }
   }

   return domIsReady;
})(domIsReady || {});

(function(document, window, domIsReady, undefined) {
    domIsReady(function() {
        document.querySelectorAll('input[type="text"]').forEach(function(e) {
            e.addEventListener('keyup', function() {
                refreshText();
                refreshFilename();
            });
            e.addEventListener('paste', function() {
                refreshText();
                refreshFilename();
            });
        });
        document.querySelectorAll('input[type="radio"]').forEach(function(e) {
            e.addEventListener('click', function() {
                refreshText();
            });
        });
        document.querySelectorAll('input[type="number"],input[type="color"]').forEach(function(e) {
            e.addEventListener('change', function() {
                refreshText();
            });
        });
        document.querySelector('#download').addEventListener('click', function() {
            var f         = document.getElementById('filename');
            this.download = f.value;
            this.href     = document.getElementById('canvas').toDataURL().replace("image/png", "image/octet-stream");
        });
        document.querySelector('#reset').addEventListener('click', function() {
            reset();
        });
        document.querySelector('#settings').addEventListener('click', function() {
            var b = document.getElementById('settings-block');
            if (b.classList.contains('hidden')) {
                b.classList.remove('hidden');
                this.text = 'Hide advanced settings';
            } else {
                b.classList.add('hidden');
                this.text = 'Show advanced settings';
            }
        });

        if (window.location.hash) {
            document.querySelector('#porn').value = decodeURIComponent(window.location.hash.substr(1));
        }
        var url = new URL(window.location);
        if (url.searchParams.get('w') !== null) {
            document.getElementById('wemake').value       = url.searchParams.get('w');
        }
        if (url.searchParams.get('wx') !== null) {
            document.getElementById('wemake-x').value     = url.searchParams.get('wx');
        }
        if (url.searchParams.get('wy') !== null) {
            document.getElementById('wemake-y').value     = url.searchParams.get('wy');
        }
        if (url.searchParams.get('ws') !== null) {
            document.getElementById('wemake-size').value  = url.searchParams.get('ws');
        }
        if (url.searchParams.get('wc') !== null) {
            document.getElementById('wemake-color').value = url.searchParams.get('wc');
        }
        if (url.searchParams.get('p') !== null) {
            document.getElementById('porn').value         = url.searchParams.get('p');
        }
        if (url.searchParams.get('px') !== null) {
            document.getElementById('porn-x').value       = url.searchParams.get('px');
        }
        if (url.searchParams.get('py') !== null) {
            document.getElementById('porn-y').value       = url.searchParams.get('py');
        }
        if (url.searchParams.get('ps') !== null) {
            document.getElementById('porn-size').value    = url.searchParams.get('ps');
        }
        if (url.searchParams.get('pc') !== null) {
            document.getElementById('porn-color').value   = url.searchParams.get('pc');
        }
        if (url.searchParams.get('co') !== null) {
            document.getElementById('color').value        = url.searchParams.get('co');
        }
        if (url.searchParams.get('rc') !== null) {
            document.getElementById('rect-color').value   = url.searchParams.get('rc');
        }
        if (url.searchParams.get('f') !== null) {
            document.getElementById('filename').value     = url.searchParams.get('f');
        }

        refreshFilename();
        refreshText();
    });
})(document, window, domIsReady);

function reset() {
    document.getElementById('wemake-x').value     = "400";
    document.getElementById('wemake-y').value     = "160";
    document.getElementById('wemake-size').value  = "150";
    document.getElementById('wemake-color').value = "#000000";
    document.getElementById('porn-x').value       = "400";
    document.getElementById('porn-y').value       = "350";
    document.getElementById('porn-size').value    = "220";
    document.getElementById('porn-size').value    = "220";
    document.getElementById('porn-color').value   = "#000000";
    document.getElementById('color').value        = "#fcd205";
    document.getElementById('rect-color').value   = "#000000";

    var corners = document.getElementsByName('corners');
    for (var i = 0; i < corners.length; i++) {
        if (corners[i].value === 20) {
            corners.checked = true;
        } else {
            corners.checked = false;
        }
    }

    refreshText();
}

function refreshFilename() {
    var w = document.getElementById('wemake');
    var p = document.getElementById('porn');
    var f = document.getElementById('filename');

    var name = w.value+' '+p.value+'.png';
    f.value = name.replace(/ /g, '_');
}

function refreshText() {
    var w    = document.getElementById('wemake');
    var wx   = document.getElementById('wemake-x');
    var wy   = document.getElementById('wemake-y');
    var ws   = document.getElementById('wemake-size');
    var wc   = document.getElementById('wemake-color');
    var p    = document.getElementById('porn');
    var px   = document.getElementById('porn-x');
    var py   = document.getElementById('porn-y');
    var ps   = document.getElementById('porn-size');
    var pc   = document.getElementById('porn-color');
    var co   = document.getElementById('color');
    var rc   = document.getElementById('rect-color');
    var c    = document.getElementById('canvas');

    var ctx  = c.getContext('2d');
        ctx.clearRect(0, 0, c.width, c.height);

    // update URL
    var url = new URL(window.location);
    url.searchParams.set('w',  w.value);
    url.searchParams.set('wx', wx.value);
    url.searchParams.set('wy', wy.value);
    url.searchParams.set('ws', ws.value);
    url.searchParams.set('wc', wc.value);
    url.searchParams.set('p',  p.value);
    url.searchParams.set('px', px.value);
    url.searchParams.set('py', py.value);
    url.searchParams.set('ps', ps.value);
    url.searchParams.set('pc', pc.value);
    url.searchParams.set('co', co.value);
    url.searchParams.set('rc', rc.value);
    window.history.pushState("change", "WemaWema", url);

    // background color
    var radius = 20;
    var corners = document.getElementsByName('corners');
    for (var i = 0; i < corners.length; i++) {
        if (corners[i].checked) {
            radius = parseInt(corners[i].value);
        }
    }
    ctx.fillStyle = co.value;
    roundRect(ctx, 0, 0, c.width, c.height, radius, true, false);

    // rounded rectangle
    ctx.lineWidth   = 20;
    ctx.strokeStyle = rc.value;
    roundRect(ctx, 25, 25, 750, 350, 20, false);

    ctx.textAlign="center";
    // Write WE MAKE
    ctx.font      = 'bold '+parseInt(ws.value)+'px sans-serif';
    ctx.fillStyle = wc.value;
    ctx.fillText(w.value, parseInt(wx.value), parseInt(wy.value), 725);

    // Write new value
    ctx.font      = 'bold '+parseInt(ps.value)+'px sans-serif';
    ctx.fillStyle = pc.value
    ctx.fillText(p.value, parseInt(px.value), parseInt(py.value), 725);
}

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke == 'undefined') {
        stroke = true;
    }
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
        for (var side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
        ctx.fill();
    }
    if (stroke) {
        ctx.stroke();
    }

}
