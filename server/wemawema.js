const http  = require('http');
const parse = require('request-form');

var url    = require('url');
var Canvas = require('canvas')
var fs     = require('fs');
var net    = require('net');

const requestHandler = (request, response) => {
    var queryData = url.parse(request.url, true).query;

    var Image     = Canvas.Image;
    var canvas    = new Canvas(800, 400);
    var ctx       = canvas.getContext('2d');

    var w  = (queryData.w  !== undefined) ? queryData.w  : 'WE MAKE';
    var wx = (queryData.wx !== undefined) ? queryData.wx : '400';
    var wy = (queryData.wy !== undefined) ? queryData.wy : '160';
    var ws = (queryData.ws !== undefined) ? queryData.ws : '150';
    var p  = (queryData.p  !== undefined) ? queryData.p  : 'PORN';
    var px = (queryData.px !== undefined) ? queryData.px : '400';
    var py = (queryData.py !== undefined) ? queryData.py : '350';
    var ps = (queryData.ps !== undefined) ? queryData.ps : '220';

    parse(request, {})
    .then(data => {
        console.log(data);
        if (data.text !== undefined) {
            response.writeHead(200, {
                'Content-Type': 'application/json'
            });
            response.end(JSON.stringify({response_type: 'in_channel', text: fullUrl(request)+'?p='+encodeURIComponent(data.text)}));
        } else {
            ctx.fillStyle = 'rgba(252, 210, 5, 1)';
            roundRect(ctx, 0, 0, 800, 400, 20, true, false);

            // rounded rectangle
            ctx.lineWidth   = 20;
            ctx.strokeStyle = 'rgb(0, 0, 0)';
            roundRect(ctx, 25, 25, 750, 350, 20, false);

            ctx.textAlign="center";
            // Write WE MAKE
            ctx.font      = 'bold '+parseInt(ws)+'px sans-serif';
            ctx.fillStyle = 'rgba(0, 0, 0, 1)';

            var i = 0;
            while (ctx.measureText(w).width > 725) {
                ctx.font      = 'bold '+(parseInt(ws) - i++)+'px sans-serif';
            }
            ctx.fillText(w, parseInt(wx), parseInt(wy));

            // Write new value
            ctx.font      = 'bold '+parseInt(ps)+'px sans-serif';
            ctx.fillStyle = 'rgba(0, 0, 0, 1)';

            i = 0;
            while (ctx.measureText(p).width > 725) {
                ctx.font      = 'bold '+(parseInt(ps) - i++)+'px sans-serif';
            }
            ctx.fillText(p, parseInt(px), parseInt(py));

            var data = canvas.toDataURL().replace('data:image/png;base64,','');

            var img = new Buffer(data, 'base64');

            response.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': img.length
            });
            response.end(img);
        }
    }, err => {
    });
}

const server = http.createServer(requestHandler)

server.listen('/tmp/wemawema.sock', (err) => {
    if (err) {
        return console.log('something bad happened', err);
    } else {
        console.log('server is listening on socket /tmp/wemawema.sock');
    }
});

server.on('error', function (e) {
    if (e.code == 'EADDRINUSE') {
        var clientSocket = new net.Socket();
        clientSocket.on('error', function(e) { // handle error trying to talk to server
            if (e.code == 'ECONNREFUSED') {  // No other server listening
                fs.unlinkSync('/tmp/wemawema.sock');
                server.listen('/tmp/wemawema.sock', (err) => {
                    if (err) {
                        return console.log('something bad happened', err)
                    }
                });
            }
        });
        clientSocket.connect({path: '/tmp/wemawema.sock'}, function() {
            console.log('Server running, giving up...');
            process.exit();
        });
    }
});

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke == 'undefined') {
        stroke = true;
    }
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
        for (var side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
        ctx.fill();
    }
    if (stroke) {
        ctx.stroke();
    }

}


function fullUrl(req) {
    return req.headers['x-forwarded-proto']+'://'+req.headers['host']+'/WeMake.png';
}
